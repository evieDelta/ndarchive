package ndarchive

import (
	"bytes"
	"strconv"
	"time"
)

// parse a Snowflake from a String ID
func parseSnowflake(id string) (Snowflake, error) {
	i, err := strconv.ParseUint(id, 10, 64)
	return Snowflake(i), err
}

type Snowflake uint64

// IsZero simply returns whether or not an ID is present or not
func (s Snowflake) IsZero() bool { return s == 0 }

// Milliseconds returns a time.Duration of the time in an ID,
// note that since ID does not contain the epoch it can only give the duration since epoch and not the absolute time,
// you can get the absolute time by adding the duration to the epoch time or via Generator.IDTime
func (s Snowflake) Milliseconds() time.Duration {
	t := s >> 22
	return time.Duration(t) * time.Millisecond
}

func (s Snowflake) String() string {
	return s.Base10()
}

// Base10 returns a base10 encoded representation of s
func (s Snowflake) Base10() string {
	return strconv.FormatUint(uint64(s), 10)
}

func (s Snowflake) MarshalJSON() ([]byte, error) {
	return []byte("\"" + s.String() + "\""), nil
}

func (s *Snowflake) UnmarshalJSON(b []byte) error {
	b = bytes.Trim(b, "'\"")
	u, err := parseSnowflake(string(b))
	*s = u

	return err
}

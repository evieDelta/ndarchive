package ndarchive

import "time"

// file standard
// all the files are to be included in a tar archive compressed with gzip
// within the archive, to make it easier on readers if the control is given by the writer
// the files should be written in the following order
// - meta.json
// - users.json
// - guild.json
// - messages.json
// manifest.json and signature can go whereever

// FileMeta - meta.json
type FileMeta struct {
	FormatVersion Version   `json:"format_version"`
	Timestamp     time.Time `json:"timestamp"`

	// List of channels actually included in the archive
	// note that not all viewers may work with multi channel mode
	Channels []Snowflake `json:"channels"`
	GuildID  Snowflake   `json:"guild_id"`

	// The creator of the archive
	ArchiveCreator User `json:"archive_creator,omitempty"`

	// Viewers that choose to handle certain Custom fields and display certain custom field values
	// in an intuitive way will need to know what custom fields to look for, since they can be anything
	// when using custom values its recommended to set a value in here to tell viewers of its presence
	// recommended to make the value something unique, such as a url to a repo describing the custom values
	Extensions []string

	// if a bot has custom information it wants to attatch, it can do so here
	Custom CustomValues `json:"custom,omitempty"`
}

// FileUsers - users.json
type FileUsers struct {
	// The list of users, the key can be anything, recommended to just make it a serial sequence
	// only needs to (and should only) contain the users actually contained in the archive
	// ideally should also distinguish webhooks of the same ID but different name or avatar
	// (so it'll function with tools like PK properly)
	Users map[uint64]User `json:"users"`
}

// FileGuild - guild.json
type FileGuild struct {
	Guild `json:"guild"`

	// Channels, only needs to contain the current channel and any channels mentioned in the archived messages
	// and optionally the parent category
	Channels map[string]Channel `json:"channels"`
	// Roles
	Roles map[string]Role `json:"roles"`
}

// FileMessages - messages-{channel_id}.json
type FileMessages struct {
	Messages []Message `json:"messages"`
}

// FileManifest - manifest.json
// contains integrity hashes of all files within the archive
// except manifest.json (itself, obvious reasons) and signature
type FileManifest struct {
	// map key is the filename (including extension)
	Hashes map[string]Hashes `json:"hashes"`
}

// Hashes is a container containing various types of integrity hash, should be encoded in RFC4648 base64 without padding
// base64.RawSTDEncoding in the go stdlib, otherwise
// https://www.rfc-editor.org/rfc/rfc4648.html#section-4
// https://rfc-editor.org/rfc/rfc4648.html#section-3.2
type Hashes struct {
	Blake3512 string `json:"blake3_512"`
	Blake3256 string `json:"blake3_256"`

	SHA3512 string `json:"sha3_512"`
	SHA3256 string `json:"sha3_256"`

	SHA512 string `json:"sha512"`
	SHA256 string `json:"sha256"`

	// Not cryptographically secure, added for completeness and just being supported by everything made in the past 2 decades
	SHA1 string `json:"sha1"`
	MD5  string `json:"md5"`

	// Checksums, not hashes
}

# Ndarchive
Ndarchive is a json format for archiving a discord channel, built as a complete rework of the older darchive format due to its limitations

Currently this new format is still beta and the format is not frozen yet

Cryptographic signature support is not yet finished or included due to complexity, for now just make a hash (ex sha256) of the full finished archive file that can be used for validation

## Viewers
Currently there are not any options for a polished or complete viewer for this archive format

## Version history
#### 1.0
 - Creation **NOT YET FROZEN**